const graphql = require('graphql')
const Product = require('./../Components/Product')

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLBoolean,
    GraphQLSchema,
    GraphQLID,
    GraphQLFloat,
    GraphQLList,
    GraphQLNonNull,
    GraphQLInputObjectType
} = graphql


const ProductType = new GraphQLObjectType({
    name: 'Product',
    fields: () => ({
        _id: {
            type: GraphQLID
        },
        sku: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString
        },
        status: {
            type: GraphQLBoolean
        },
        description: {
            type: GraphQLString
        },
        price: {
            type: GraphQLFloat
        },
        qty: {
            type: GraphQLFloat
        },
        seller: {
            type: GraphQLString
        }
    })
})

const ProductInputType = new GraphQLInputObjectType({
    name: 'ProductInput',
    fields: () => ({
        _id: {
            type: GraphQLID
        },
        sku: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString
        },
        status: {
            type: GraphQLBoolean
        },
        description: {
            type: GraphQLString
        },
        price: {
            type: GraphQLFloat
        },
        qty: {
            type: GraphQLFloat
        },
        seller: {
            type: GraphQLString
        }
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        product: {
            type: ProductType,
            args: {
                id: {
                    type: GraphQLID
                },
                name: {
                    type: GraphQLString
                }
            },
            resolve(parent, args) {
                if (args.name) {
                    return Product.find({
                        name: args.name
                    });
                }
                return Product.findById(args.id);
            }
        },
        products: {
            type: new GraphQLList(ProductType),
            resolve(parent, args) {
                return Product.find({});
            }
        }
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addProduct: {
            type: ProductType,
            description: "Realiza a criação de um produto", 
            args: {
                seller: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                sku: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                name: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                description: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                qty: {
                    type: new GraphQLNonNull(GraphQLFloat)
                },
                status: {
                    type: new GraphQLNonNull(GraphQLBoolean)
                },
                price: {
                    type: new GraphQLNonNull(GraphQLFloat)
                },
            },
            resolve(parent, args) {
                let product = new Product({
                    seller: args.seller,
                    sku: args.sku,
                    name: args.name,
                    description: args.description,
                    qty: args.qty,
                    status: args.status,
                    price: args.price
                });
                return product.save();
            }
        },
        updateProduct: {
            type: ProductType,
            description: "Realiza a atualização de um produto", 
            args: {
                _id: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                product: {
                    type: ProductInputType
                }
            },
            resolve: (parent, { _id, product }) => {
                Product.findOneAndUpdate({ _id }, {$set: product}, {
                    upsert: false
                })
            }
        },
        deleteProduct: {
            type: ProductType,
            description: "Realiza a atualização de um produto", 
            args: {
                _id: {
                    type: new GraphQLNonNull(GraphQLString)
                },
            },
            resolve: (parent, { _id, product }) => {
                return Product.deleteOne({ _id })
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});