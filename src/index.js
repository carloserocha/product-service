const express = require('express')
const graphqlHTTP = require('express-graphql')

const mongo = require('mongoose')
const app = express()

mongo.connect('mongodb://localhost:17017/drugstore-product', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true
})

mongo.connection.once('open', () => {
    console.log('connected to database')
})

app.use('/graphiql', graphqlHTTP ({ 
    schema: require('./Model/schema.js'), 
    graphiql: true
}))

app.listen(8080, () => {
    console.log('🚀 running succefully!!!')
})