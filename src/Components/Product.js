const mongo = require('mongoose')

const Product = new mongo.Schema ({
    seller: {
        type: String,
        required: true,
    },
    sku: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    status: {
        type: Boolean,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    qty: {
        type: Number,
        required: true,
    }
})

module.exports = mongo.model('product', Product)